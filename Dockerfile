FROM alpine:3.12

COPY --from=openpolicyagent/opa /opa /usr/bin/opa

USER 0

CMD ["/bin/sh"]
